<?php

namespace Itdotmedia\Connector\Helper;

class StoreHandler extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_storeRepository;
	
	public function __construct( 
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Store\Model\StoreRepository $_storeRepository)
	{
		$this->_storeRepository = $_storeRepository;
  }
	
	/**
	* {@inheritdoc}
	*/
	public function getStores()
	{
		$stores = $this->_storeRepository->getList();
		$websiteIds = array();
		$storeList = array();
		foreach ($stores as $store) {
				$websiteId = $store["website_id"];
				$storeId = $store["store_id"];
				$storeName = $store["name"];
				$storeList[$storeId] = $storeName;
				array_push($websiteIds, $websiteId);
		}
		return json_encode($storeList);
	}

}