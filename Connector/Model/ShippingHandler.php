<?php
namespace Itdotmedia\Connector\Model;

class ShippingHandler
	extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
	implements \Itdotmedia\Connector\Api\ShippingHandlerInterface
{
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager = null;
	
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $_scopeConfig = null;

	/**
	 * @var \Magento\Shipping\Model\Config
	 */
	protected $_shippingConfig = null;
	 
	/**
	 * 
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Magento\Shipping\Model\Config $shipconfig
	 */
	public function __construct( 
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Shipping\Model\Config $shippingConfig
	) {
		$this->_storeManager = $storeManager;
		$this->_scopeConfig = $scopeConfig;
		$this->_shippingConfig = $shippingConfig;
	}
	
	/**
	* {@inheritdoc}
	*/
  public function getAllOptions($withEmpty = true) {

	if (!$this->_options) {
			$storeId = $this->_storeManager->getStore()->getId();
			
			$activeCarriers = $this->_shippingConfig->getActiveCarriers($storeId);
			foreach($activeCarriers as $carrierCode => $carrierModel) {
				$options = array();
				if( $carrierMethods = $carrierModel->getAllowedMethods() ) {
					$carrierTitle =$this->_scopeConfig->getValue('carriers/'.$carrierCode.'/title');
					foreach ($carrierMethods as $methodCode => $method) {
						$this->_options[] = [
							'id'  	=> $carrierCode.'_'.$methodCode,
							'code'  => $carrierCode.'_'.$methodCode,
							'label' => $carrierTitle . ' ' . $method
						];
					}
				}
			}
		}
		
		return $this->_options;
	}

}