<?php

namespace Itdotmedia\Connector\Model;

use Magento\Tax\Model\ClassModel;
use Magento\Tax\Api\TaxClassManagementInterface;
use Magento\Tax\Api\TaxCalculationInterface;


class TaxHandler	
	extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
	implements \Itdotmedia\Connector\Api\TaxHandlerInterface
{
	/**
	 * @var \Magento\Tax\Model\ResourceModel\TaxClass\CollectionFactory
	 */
	protected $_classesFactory = null;
	
	/**
	 * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory
	 */
	protected $_optionFactory = null;

	/**
	 * @var \Magento\Tax\Api\TaxClassRepositoryInterface
	 */
	protected $_taxClassRepository = null;
	
	/**
	 * @var \Magento\Tax\Api\TaxCalculationInterface
	 */
	protected $_taxCalculation = null;
	
	/**
	 * @var \Magento\Tax\Model\Calculation\Rate
	 */
	protected $_taxRate = null;
	
	/**
	 * @var \Magento\Framework\Api\FilterBuilder
	 */
	 protected $_filterBuilder = null;
	
	/**
	 * @var \Magento\Framework\Api\SearchCriteriaInterface
	 */
	 protected $_searchCriteriaBuilder = null;
	
	/**
	 * 
	 * @param \Magento\Tax\Model\ResourceModel\TaxClass\CollectionFactory $classesFactory
	 * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $optionFactory
	 * @param \Magento\Tax\Api\TaxClassRepositoryInterface $taxClassRepository
	 * @param \Magento\Tax\Api\TaxRuleRepositoryInterface $taxRuleRepository
	 * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculation
	 * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
	 * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
	 */
	public function __construct( 
		\Magento\Tax\Model\ResourceModel\TaxClass\CollectionFactory $classesFactory,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $optionFactory,
		\Magento\Tax\Api\TaxClassRepositoryInterface $taxClassRepository,
		\Magento\Tax\Api\TaxRuleRepositoryInterface $taxRuleRepository,
		\Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
		\Magento\Tax\Model\Calculation\Rate $taxRate,
		\Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
		\Magento\Framework\Api\FilterBuilder $filterBuilder
	) {
		$this->_classesFactory = $classesFactory;
		$this->_optionFactory = $optionFactory;
		$this->_taxClassRepository = $taxClassRepository;
		$this->_taxRuleRepository = $taxRuleRepository;
		$this->_taxCalculation = $taxCalculation;
		$this->_taxRate = $taxRate;
		$this->_filterBuilder = $filterBuilder;
		$this->_searchCriteriaBuilder = $searchCriteriaBuilder;
	}
	
	/**
	* {@inheritdoc}
	*/
  public function getAllOptions($withEmpty = true)
	{
  	
		if (!$this->_options) {
			
			$taxRates = $this->_taxRate->getCollection()->getData();
			foreach ($taxRates as $tax) {
				$this->_options['TaxRates'][$tax['tax_calculation_rate_id']] = [
						'taxId' 	=> $tax['tax_calculation_rate_id'],
						'taxType'	=> 'TAX_RATE',
						'label' 	=> $tax["code"],
						'rate' 		=> $tax["rate"]
				];
			}

			$filter = $this->_filterBuilder
								->setField(ClassModel::KEY_TYPE)
								->setValue(TaxClassManagementInterface::TYPE_PRODUCT)
								->create();
			$searchCriteria = $this->_searchCriteriaBuilder->addFilters([$filter])->create();
			
			$searchResults = $this->_taxClassRepository->getList($searchCriteria);
			foreach ($searchResults->getItems() as $taxClass) {
				$extensionAttributes = $taxClass->getExtensionAttributes();
				$this->_options['TaxClassRepository'][$taxClass->getClassId()] = [
						'taxId' 	=> $taxClass->getClassId(),
						'taxType' => $taxClass->getClassType(),
						'label' 	=> $taxClass->getClassName(),
						'rate' 		=> json_encode($extensionAttributes)
				];
			}
			
			$searchCriteria = $this->_searchCriteriaBuilder->create();
			$searchResults = $this->_taxRuleRepository->getList($searchCriteria);
			foreach ($searchResults->getItems() as $taxClass) {
				$this->_options['TaxRuleRepository'][] = [
						'ruleId' 	=> $taxClass->getId(),
						'ruleCode' => $taxClass->getCode(),
						'taxClassId' => $taxClass->getProductTaxClassIds(),
						'taxClassRateId' => $taxClass->getTaxRateIds()
				];
			}
			
			$this->_options['Tax'] = [];
			foreach($this->_options['TaxRuleRepository'] as $item) {
				$this->_options['Tax'][] = [
					'taxId' 	=> $item['ruleId'],
					'label' 	=> $this->_options['TaxRates'][$item['taxClassRateId'][0]]['label'],
					'rate' 		=> $this->_options['TaxRates'][$item['taxClassRateId'][0]]['rate']
				];
			}
			
		}
		
		return $this->_options['Tax'];
	}

}