<?php
namespace Itdotmedia\Connector\Model\extensions;
use Itdotmedia\Connector\Api\extensions\ShippingPickupInterface;
use Magento\Framework\App\ResourceConnection;

class ShippingPickupHandler
	implements ShippingPickupInterface
{
	
	/** @var ResourceConnection */
	private $resourceConnection;
    	
	public function __construct(ResourceConnection $resourceConnection)
	{
		$this->resourceConnection = $resourceConnection;
	}	
	
	/**
	* {@inheritdoc}
	*/
  public function getAllOptions() {
		$data = [];
  
		$TABLE = $this->resourceConnection->getTableName('instorepickuppremium_retailstores');
		$connection = $this->resourceConnection->getConnection();
		$query = $connection->query("SHOW TABLES LIKE '" . $TABLE . "'");
		if (count($query->fetchAll()) > 0) {
			$query = $connection->query("SELECT retailstore_id, updated_at FROM " . $TABLE);
			while ($rec = $query->fetch()) {
				$data[] = $rec;
			}		
		} 
		
		return $data;
	}
	
	/**
	* {@inheritdoc}
	*/
  public function getProperties($retailstore_id) {
		$data = [];
  
		$TABLE = $this->resourceConnection->getTableName('instorepickuppremium_retailstores');
		$connection = $this->resourceConnection->getConnection();
		$query = $connection->query("SHOW TABLES LIKE '" . $TABLE . "'");
		if (count($query->fetchAll()) > 0) {
			$query = $connection->query("SELECT * FROM " . $TABLE . " WHERE retailstore_id = " . $retailstore_id);
			while ($rec = $query->fetch()) {
				$data[] = $rec;
			}		
		} 
		
		return $data;
	}

}