<?php
namespace Itdotmedia\Connector\Model\extensions;
use Itdotmedia\Connector\Api\extensions\ShippingMatrixrateInterface;
use Magento\Framework\App\ResourceConnection;

class ShippingMatrixrateHandler
	implements ShippingMatrixrateInterface
{
	
	/** @var ResourceConnection */
	private $resourceConnection;
    	
	public function __construct(ResourceConnection $resourceConnection)
	{
		$this->resourceConnection = $resourceConnection;
	}	
	
	/**
	* {@inheritdoc}
	*/
  public function getAllOptions($withEmpty = true) {
  
		$TABLE_WEBSHOPAPPS_MATRIXRATE = $this->resourceConnection->getTableName('webshopapps_matrixrate');
		$connection = $this->resourceConnection->getConnection();
		$data = [];

		$query = $connection->query("SHOW TABLES LIKE '" . $TABLE_WEBSHOPAPPS_MATRIXRATE . "'");
		if (count($query->fetchAll()) > 0) {
			$query = $connection->query("SELECT * FROM " . $TABLE_WEBSHOPAPPS_MATRIXRATE);
			while ($rec = $query->fetch()) {
				$data[] = $rec;
			}		
		} 
		
		return $data;
	}
}