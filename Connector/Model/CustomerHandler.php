<?php
namespace Itdotmedia\Connector\Model;

class CustomerHandler
	extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
	implements \Itdotmedia\Connector\Api\CustomerHandlerInterface
{
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager = null;
	
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $_scopeConfig = null;

	/**
	 * @var Magento\Customer\Model\ResourceModel\Group\Collection
	 */
	protected $_groupCollection = null;
	
	/**
	 * @var Magento\Eav\Model\Config
	 */
	protected $_eavConfig= null;
	
	 
	/**
	 * 
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Magento\Customer\Model\ResourceModel\Group\Collection $groupCollection
	 * @param \Magento\Eav\Model\Config $eavConfig
	 */
	public function __construct( 
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Customer\Model\ResourceModel\Group\Collection $groupCollection,
		\Magento\Eav\Model\Config $eavConfig
	) {
		$this->_storeManager = $storeManager;
		$this->_scopeConfig = $scopeConfig;
		$this->_groupCollection = $groupCollection;
		$this->_eavConfig = $eavConfig;
	}
	
	/**
	* {@inheritdoc}
	*/
	public function getAllOptions() {
		// nothing
	}

	/**
	* {@inheritdoc}
	*/
  public function getAllGroupOptions() {

		if (!$this->_options) {
			$storeId = $this->_storeManager->getStore()->getId();
			
			$orderStatusCodes= $this->_groupCollection->toOptionArray();

			foreach($orderStatusCodes as $orderStatusCode) {
				$this->_options[] = [
					'id'  	=> $orderStatusCode['value'],
					'code'	=> $orderStatusCode['value'],
					'label'	=> $orderStatusCode['label']
				];
			}
		}
		
		return $this->_options;
	}
	
	/**
	* {@inheritdoc}
	*/
  public function getAllAttributeGender() {

		if (!$this->_options) {
			$storeId = $this->_storeManager->getStore()->getId();
			
			$attribute = $this->_eavConfig->getAttribute('customer','gender');

			$options = $attribute->getSource()->getAllOptions();
			
			foreach($options as $option) {
				if(!empty($option['value'])) {
					$this->_options[] = [
						'id'  	=> $option['value'],
						'code'	=> $option['value'],
						'label'	=> $option['label']
					];
				}
			}
		}
		
		return $this->_options;
	}

}