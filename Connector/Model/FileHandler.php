<?php
namespace Itdotmedia\Connector\Model;

class FileHandler
	implements \Itdotmedia\Connector\Api\FileHandlerInterface
{
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager = null;
	
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $scopeConfig = null;

	/**
	 * @var Magento\Framework\App\RequestInterface
	 */
	protected $request= null;
	
	/**
	 * @var Magento\Framework\Filesystem\DirectoryList
	 */
	protected $directoryList= null;
	 
	/**
	 * 
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Magento\Framework\App\RequestInterface $request
	 * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
	 */
	public function __construct( 
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Framework\App\RequestInterface $request,
		\Magento\Framework\Filesystem\DirectoryList $directoryList		
	) {
		$this->storeManager = $storeManager;
		$this->scopeConfig = $scopeConfig;
		$this->request = $request;
		$this->directoryList = $directoryList;
	}
	
	/**
	* {@inheritdoc}
	*/
	public function storeAttachments() {
		// get raw data
		$post = json_decode(file_get_contents('php://input'), true);
		$basePath = $this->directoryList->getPath('media');

		$retVal = [];
		foreach($post as $fileId => $fileItem) {
			$path = !empty($fileItem['path']) ? $fileItem['path'] : '/datasheets';
			$statusMsg = [
				'path' => $path,
				'file' => $fileItem['uid']
			];
			
			$statusMsg['status'] = (file_put_contents($basePath . $path . '/' . $fileItem['uid'], base64_decode($fileItem['base64_encoded_data'])) ) ? 'success' : 'failed';
			$retVal[] = $statusMsg;
		}
		return json_encode($retVal);
	}

}