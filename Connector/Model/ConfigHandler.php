<?php
namespace Itdotmedia\Connector\Model;

class ConfigHandler
	implements \Itdotmedia\Connector\Api\ConfigHandlerInterface
{
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager = null;
	
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $scopeConfig = null;
	 
	/**
	 * 
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 */
	public function __construct( 
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	) {
		$this->storeManager = $storeManager;
		$this->scopeConfig = $scopeConfig;
	}
	
	/**
	* {@inheritdoc}
  */
  public function getValue($sectionId, $groupId, $fieldId) {
		return $this->scopeConfig->getValue(implode('/', [$sectionId, $groupId, $fieldId]), \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->storeManager->getStore()->getStoreId());
	}

}