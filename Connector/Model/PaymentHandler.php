<?php

namespace Itdotmedia\Connector\Model;

class PaymentHandler
	extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
	implements \Itdotmedia\Connector\Api\PaymentHandlerInterface
{
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager = null;
	 
	 /**
	 * @var \Magento\Payment\Api\PaymentMethodListInterface
	 */
	 protected $_paymentMethodList = null;
	
	/**
	 * 
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Payment\Api\PaymentMethodListInterface $paymentMethodList
	 */
	public function __construct( 
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Payment\Api\PaymentMethodListInterface $paymentMethodList
	) {
		$this->_storeManager = $storeManager;
		$this->_paymentMethodList = $paymentMethodList;
	}
	
	/**
	* {@inheritdoc}
	*/
  public function getAllOptions($withEmpty = true) {
		if (!$this->_options) {
			
			$storeId = $this->_storeManager->getStore()->getId();
			$paymentMethodes = $this->_paymentMethodList->getList($storeId);
			foreach ($paymentMethodes as $paymentItem) {
				$this->_options[] = [
						'id'						=> $paymentItem->getCode(),
						'key'						=> $paymentItem->getCode(),
						'storeId'				=> $paymentItem->getStoreId(),
						'paymentCode' 	=> $paymentItem->getCode(),
						'paymentTitle' 	=> $paymentItem->getTitle(),
						'paymentActive' => $paymentItem->getIsActive()
				];
			}
		}
		
		return $this->_options;
	}

}