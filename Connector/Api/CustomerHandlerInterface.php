<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Itdotmedia\Connector\Api;

/**
 * @api
 * @since 0.1.0
 */
interface CustomerHandlerInterface
{
	
	/**
	* Get list of all customer groups
	*
	* @return []
	*/
	public function getAllGroupOptions();
	
	/**
	* Get list of all customer gender options
	*
	* @return []
	*/
	public function getAllAttributeGender();
	
}
