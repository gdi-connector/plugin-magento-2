<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Itdotmedia\Connector\Api;

/**
 * @api
 * @since 0.1.0
 */
interface FileHandlerInterface
{
	
	/**
	* store Attachments on filesystem
	*
	* @return void
	*/
	public function storeAttachments();
	
}
