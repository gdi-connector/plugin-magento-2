<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Itdotmedia\Connector\Api\extensions;

/**
 * @api
 * @since 0.1.0
 */
interface ShippingPickupInterface
{
    /**
     * Get info about extension
     *
     * @return []
     */
    public function getAllOptions();
		
    /**
     * Get info about pickup Item
     *
     * @param string $retailstore_id
     * @return mixed
     */
    public function getProperties($retailstore_id);
		
		
}
