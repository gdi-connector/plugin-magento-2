<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Itdotmedia\Connector\Api\extensions;

/**
 * @api
 * @since 0.1.0
 */
interface ShippingMatrixrateInterface
{
    /**
     * Get info about extension
     *
     * @return []
     */
    public function getAllOptions();
		
}
