<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Itdotmedia\Connector\Api;

/**
 * @api
 * @since 0.1.0
 */
interface ConfigHandlerInterface
{
	
	/**
	 * Get Value from Config Table
	 *
   * @param string $sectionId
   * @param string $groupId
   * @param string $fieldId
	 * @return string
	 */
	public function getValue($sectionId, $groupId, $fieldId);
	
}
