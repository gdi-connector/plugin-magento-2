Das Plugin dient als Erweiterung zur on-board REST API von Magento v2.

Es werden zuusätzlich benötigte Informationen, i.d.R. Meta Daten, vom shop abgefragt. Für die Ausführung/Abfrage werden ADMIN Rechte benötigt.

### Version ###

* Version: 1.0.0
* getestet in Magento: 2.3.0

### Plugin in Magento implementieren ###

* Installation im Verzeichnis [MAGENTO_ROOT]\app\code\Itdotmedia\